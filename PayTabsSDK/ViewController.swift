//
//  ViewController.swift
//  PayTabsSDK
//
//  Created by Diaa SAlAm on 2/25/21.
//

import UIKit

class ViewController: UIViewController {
 
    var initialSetupViewController: PTFWInitialSetupViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction
    func didTappedPay(_ sender: Any) {
        payTabsPayment()
    }


}
//MARK: PayTaps Payment
extension ViewController {

    func payTabsPayment(){
      let bundle = Bundle(url: Bundle.main.url(forResource: "Resources", withExtension: "bundle")!)
       self.initialSetupViewController = PTFWInitialSetupViewController.init(
           bundle: bundle,
           andWithViewFrame: self.view.frame,
           andWithAmount: 1.0,
           andWithCustomerTitle: "PayTabs Sample App",
           andWithCurrencyCode: "EGP",
           andWithTaxAmount: 0.0,
           andWithSDKLanguage: "en",
           andWithShippingAddress: "Manama",
           andWithShippingCity: "Manama",
           andWithShippingCountry: "BHR",
           andWithShippingState: "Manama",
           andWithShippingZIPCode: "123456",
           andWithBillingAddress: "Manama",
           andWithBillingCity: "Manama",
           andWithBillingCountry: "BHR",
           andWithBillingState: "Manama",
           andWithBillingZIPCode: "12345",
           andWithOrderID: "12345",
           andWithPhoneNumber: "00201208885424",
           andWithCustomerEmail: "diaagen2@icloud.com",
           andIsTokenization: false,
           andIsPreAuth: false,
           andWithMerchantEmail: "diaagen2@icloud.com",
        andWithMerchantSecretKey: "", //MARK: get your  Secret Key from back-end ------------------------------------------
        andWithMerchantRegion: "emirates", //egypt, saudi, oman, jordan
           andWithAssigneeCode: "SDK",
           andWithThemeColor:UIColor.red,
           andIsThemeColorLight: true)


       self.initialSetupViewController.didReceiveBackButtonCallback = {

       }

       self.initialSetupViewController.didStartPreparePaymentPage = {
         // Start Prepare Payment Page
         // Show loading indicator
       }
       self.initialSetupViewController.didFinishPreparePaymentPage = {
         // Finish Prepare Payment Page
         // Stop loading indicator
       }

        self.initialSetupViewController.didReceiveFinishTransactionCallback = {(responseCode, result, transactionID, tokenizedCustomerEmail, tokenizedCustomerPassword, token, transactionState, statementReference, traceCode) in
            print("Response Code: \(responseCode)")
            print("Response Result: \(result)")
            print("Statement Reference: \(statementReference)");
            print("Trace Code: \(traceCode)");
            
            // In Case you are using tokenization
            print("Tokenization Cutomer Email: \(tokenizedCustomerEmail)");
            print("Tokenization Customer Password: \(tokenizedCustomerPassword)");
            print("Tokenization Token: \(token)");
        }

       self.view.addSubview(initialSetupViewController.view)
       self.addChild(initialSetupViewController)

       initialSetupViewController.didMove(toParent: self)
        
    }
    
   
}


